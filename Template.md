# Guidance Pattern Template
Our proposed DMP guidance template is built on the software design pattern concept. To meet relevant requirements for DMP-related activities, we adapted several characteristics (pattern attributes) facilitating a structured management and discovery of guidance information for specific use cases and disciplines. The following list provides attribute names and brief descriptions:


- **Guidance name** * A descriptive and unique name that helps in identifying and referring to the guidance pattern
- **Motivation / Intent / Aim of guidance** * The goal behind the guidance pattern and the reason for using it
- **Recommended activities** *  A description of what should be implemented and how.
- **Example / Use case** An exemplary scenario in which this guidance pattern can be used.
- **Context** The situation in which the guidance pattern is usable including information about (see detailed explanations below):

- .disciplines 
- .funder 
- .templates 
- .keywords (see below)

- **(Rationale)** A sound justification for the recommended guidance.
- **Consequences / Costs** A description of the results, side effects, and trade-offs caused by using the guidance pattern.
- **Literature** Further readings relevant for the guidance pattern, including best practices, detailed guidance documents.
- **(Participants)** (A listing of roles involved in the recommended activities)
- **Related guidance** Further related guidance pattern(s), e.g. a similar or dependent/required pattern. 

Characteristics marked with * are mandatory, describing either name, problem or solution. The other characteristics are optional and should be described if useful. 

To describe the context of the pattern, we propose the following initial list of potential keywords. The list is based on consultation and project experiences of the authors and is meant to be extended, if needed. Since the keywords support discovery, sorting, and filtering of patterns, we suggest either describing the relevant disciplines as well as data collection methods, and major data / project properties:

- **Disciplines**: To provide a commonly tagged list of patterns, we propose to choose one or more relevant disciplines from the DFG subject area list . If the pattern is relevant for several disciplines, it should be tagged as generic
-	Type of data: We suggest to select the type of data for the created data of the pattern use case
â	observational data
â	audio-visual data
â	textual data 
â	experimental data
â	simulation data
â	derived data
-	**Collection methods**: The method on how to collect data strongly relates to data processing solutions and privacy/security aspects. We suggest to choose from the following list
â	interview
â	field observation
â	simulation
â	archival work
â	survey
â	experimental measurement
â	web harvesting 
â	crowd sourcing / citizen science
â	secondary data collection (re-use of existing datasets)
-	**Project properties**: The project properties can relate to specific guidance, e.g. big data can only be published in certain repositories; industry collaboration sometimes requires embargo times. However, we propose to use major / relevant project properties, if needed, e.g.
â	personal / sensitive data
â	big data
â	industry collaboration
â	clinical data

